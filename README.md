# Ladrillo

A stern font that nevertheless still knows how to have fun.

Made in the [Type:Bits workshop](https://typebits.gitlab.io) at the ESAPA in Avilés, 2017.

![Font preview](https://gitlab.com/typebits/font-ladrillo/-/jobs/artifacts/master/raw/Ladrillo-Regular.png?job=build-font)
  
[See it in action](https://typebits.gitlab.io/font-ladrillo/)

Download formats:

* [TrueType (.ttf)](https://gitlab.com/typebits/font-ladrillo/-/jobs/artifacts/master/raw/Ladrillo-Regular.ttf?job=build-font)
* [OpenType (.otf)](https://gitlab.com/typebits/font-ladrillo/-/jobs/artifacts/master/raw/Ladrillo-Regular.otf?job=build-font)
* [FontForge (.sfd)](https://gitlab.com/typebits/font-ladrillo/-/jobs/artifacts/master/raw/Ladrillo-Regular.sfd?job=build-font)

# Authors

* Salomé
* Marlén
* Carol Pravia
* Hugo Córdoba

# License

This font is made available under the [Open Font License](https://scripts.sil.org/OFL).
